# encoding: utf-8

from collections import deque
from multiprocessing import Process, Queue
import LCD_proc
import LCD
import random
import time

if __name__ == '__main__':
    q1 = Queue()
    q2 = Queue()
    q3 = Queue()
    q4 = Queue()

    q1.put('Hello world!!!')

    LCD_proc.LCD_runner(q1, q2, q3, q4)
    
    print 'TEST 1'
    q1.put('Hello world!')
    print 'TEST 2'

    for i in range(4):
    	time.sleep(4)
    	print str(random.randint(1,100))
    	q1.put(str(random.randint(1,100)))
    	q2.put(str(random.randint(1,100)))
    	q3.put(str(random.randint(1,100)))
    	q4.put(str(random.randint(1,100)))

	time.sleep(2)
	q1.put('End...')