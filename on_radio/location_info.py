import pyipinfodb
import requests
import os
from settings import ipinfodb_apikey as apikey

def my_ip():
    r = requests.get(r'http://jsonip.com')
    return r.json()['ip']

def get_gmtoffset(ip):
    foo = pyipinfodb.IPInfo(apikey)
    bar = foo.GetCity(ip, True) # 'True' for timezone
    return bar['Gmtoffset']

def my_gmtoffset():
    return get_gmtoffset( my_ip() )

def my_city():
    # To get ip info...
    foo = pyipinfodb.IPInfo(apikey)
    ip = my_ip() # Get IP
    bar = foo.GetCity(ip)
    return bar['City']

def set_my_timezone():
    # To get ip info...
    foo = pyipinfodb.IPInfo(apikey)
    ip = my_ip() # Get IP
    bar = foo.GetCity(ip, True) # 'True' for timezone
    tz = bar['TimezoneName']
    tz_command = 'sudo cp /usr/share/zoneinfo/%s /etc/localtime' % tz
    os.system(tz_command)

def my_timezone():
    # To get ip info...
    foo = pyipinfodb.IPInfo(apikey)
    ip = my_ip() # Get IP
    bar = foo.GetCity(ip, True) # 'True' for timezone
    return bar['TimezoneName']


if __name__ == '__main__':
    print(my_city())
