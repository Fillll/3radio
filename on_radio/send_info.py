# encoding:utf-8

from sender import seft
from temperature import my_temp
from RPi_stats import getCPUtemperature
import time
from settings import email_to, email_from

def send_all(iter=0):
    iter += 1
    text = "CPU temp: %s;\n%s\n%d°C" % ( getCPUtemperature(), time.strftime('%A %H:%M:%S %Z; %B %d, %Y'),  my_temp() )
    seft(email_to, email_from, 'RPi-temp', text)

if __name__ == '__main__':
    send_all()
