# encoding:utf-8

# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

import time

def send_email_from_file(To, From, Subject, BodyFile): # Capital for 'from'
    # Open a plain text file for reading.  For this example, assume that
    # the text file contains only ASCII characters.
    fp = open(BodyFile, 'rb')
    # Create a text/plain message
    msg = MIMEText(fp.read())
    fp.close()
    
    msg['Subject'] = Subject
    msg['From'] = From
    msg['To'] = To
    
    s = smtplib.SMTP('localhost')
    s.sendmail(From, [To], msg.as_string())
    s.quit()

def send_email_from_text(To, From, Subject, Text): # Capital for 'from'
    msg = MIMEText(Text)
    
    msg['Subject'] = Subject
    msg['From'] = From
    msg['To'] = To
    
    s = smtplib.SMTP('localhost')
    s.sendmail(From, [To], msg.as_string())
    s.quit()

def seft(To, From, Subject, Text, iter=0):
    iter += 1
    try:
        Subject += ' <%d>' % iter
        send_email_from_text(To, From, Subject, Text)
    except:
        if iter < 10:
            time.sleep(2*iter)
            seft(To, From, Subject, Text, iter)
        else:
            print('3Radio> in SEFT failed')

if __name__ == '__main__':
    send_email_from_text('rpi-spam@fillll.ru', 'rpi-stats@fillll.ru', 'Test mail', 'It works!')
    send_email_from_file('rpi-spam@fillll.ru', 'rpi-stats@fillll.ru', 'Test mail', 'textfile')
