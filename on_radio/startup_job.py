# encoding: utf-8

import time
import json
from settings import email_to, email_from, ipinfodb_apikey
from sender import seft
from temperature import my_temp
import RPi_stats
import location_info

import hardware.LCD as LCD

def collect_info():
    data = {}
    data['city']     = location_info.my_city()
    data['ip']       = location_info.my_ip()
    data['timezone'] = location_info.my_timezone()
    return data

def compose_email(data):
    text = '\nLOCATION\n========\n'
    text += 'My current city is %s.\n' % data['city']
    text += 'Current temperature in %s is %s C.\n' % ( data['city'], my_temp() )
    text += '\nTIME\n====\n'
    text += 'My timezone is %s.\n' % data['timezone']
    text += 'Old time is %s.\n' % time.strftime('%A %H:%M:%S %Z; %B %d, %Y')
    # Set current timezone
    location_info.set_my_timezone() # <<<< Timezone!
    text += 'New time is %s.\n' % time.strftime('%A %H:%M:%S %Z; %B %d, %Y')
    text += '\nE-MAILS\n=======\n'
    text += 'E-mail to %s, from %s.\n' % ( email_to, email_from )
    text += '\nIP\n==\n'
    text += 'My IP is %s.\n' % data['ip']
    text += 'API key for IPInfoDB is %s.\n' % ipinfodb_apikey
    text += '\nRPi Stats\n=========\n'
    text += 'Current CPU temperature is %s C.\n' % RPi_stats.getCPUtemperature()
    return text

def write_info(data):
    with open('/tmp/data.txt', 'w') as outfile:
        json.dump(data, outfile)

def on_startup():
    time.sleep(10)
    # Work with LCD
    LCD.start_LCD(LCD.LCD_E, LCD.LCD_RS, LCD.LCD_D4, LCD.LCD_D5, LCD.LCD_D6, LCD.LCD_D7, LCD.LED_ON)
    LCD.center_text("", "3Radio", "------", "")
    
    # Check internet! If no, say no!
    # .............
    
    # Collect settings
    # and other information
    data = collect_info()
    write_info(data)
    
    # Compose an e-mail, and while
    # composing set a timezone
    text = 'Hi!\n%s\n--\nYour 3Radio' % compose_email(data)
    # After all send e-mail
    seft(email_to, email_from, '3Radio Booted!', text)
    
    LCD.line4_center("Loaded")

if __name__ == '__main__':
    on_startup()
