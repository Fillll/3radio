from json import load
from urllib2 import urlopen
from pprint import pprint
from location_info import my_city

def my_temp():
    q = 'http://openweathermap.org/data/2.1/find/name?q=%s' % my_city()
    data = urlopen(q)
    cities = load(data)
    if cities['count'] > 0:
        city = cities['list'][0]
        temp = city['main']['temp'] - 273.15
        return round(temp)

if __name__ == '__main__':
    print my_temp()
